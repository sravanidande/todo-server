const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require('./routes/api');
const path = require('path');
require('dotenv').config();
app.use(bodyParser.json());

app.use('/api', routes);

mongoose
  .connect(process.env.DB, { useNewUrlParser: true })
  .then(() => console.log(`Datebase connected successfully`))
  .catch(err => console.log(err));

app.use((req, res) => {
  res.send('bye world');
});

app.listen(3000, () => console.log('listening to port 3000...'));

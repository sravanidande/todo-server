const express = require('express');
const router = express.Router();
const Todo = require('../models/todo');

router.get('/todos', (req, res) => {
  res.send(Todo);
});

router.get('/todos/:id', (req, res) => {
  const todo = Todo.find(ci => ci.id === parseInt(req.params.id));
  if (!todo) {
    res.status(404).send('The todo with the given id is not found');
  } else {
    res.send(todo);
  }
});

router.post('/todos', (req, res) => {
  const todo = {
    action: res.body.name
  };
  Todo.push(todo);
  res.send(todo);
});

router.delete('/todos/:id', (req, res, next) => {
  const item = Todo.findIndex(ci => ci.id === parseInt(req.params.id));
  Todo.splice(item, 1);
  res.send(Todo);
});

module.exports = router;
